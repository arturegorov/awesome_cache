require './stores/base_store.rb'

Dir[File.expand_path('./stores/*.rb', File.dirname(__FILE__))].each do |file|
  require file
end

class AwesomeCache
  attr_reader :memory_store_max_size, :file_store_max_size

  def initialize(memory_store_max_size, file_store_max_size)
    @memory_store_max_size = memory_store_max_size
    @file_store_max_size = file_store_max_size
  end

  def set(key, value)
    return unless get(key).nil?
    first_store = stores.first
    begin
      first_store.set(key, value)
    rescue Stores::CacheFull
      # В задаче описан вариант с 2 уровнями кеша, поэтому код ниже мог бы быть другим.
      # Этот код позволяет без редактирования метода set увеличивать кол-во уровней
      prev_store = first_store
      stores[1..-1].each do |store|
        current_key, current_value = prev_store.cleanup.to_a.flatten
        begin
          store.set(current_key, current_value)
          break
        rescue Stores::CacheFull
          prev_store = store
          if store == stores.last
            store.cleanup
            retry
          else
            next
          end
        end
      end
      retry
    end
  end

  def get(key)
    stores.each do |store|
      value = store.get(key)
      return value if value
    end
    nil
  end

  def clear
    stores.each(&:clear)
    nil
  end

  private

  def stores
    @stores ||= [Stores::MemoryStore.new(memory_store_max_size),
                 Stores::FileStore.new(file_store_max_size)]
  end
end
