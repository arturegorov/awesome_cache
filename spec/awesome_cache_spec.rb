require 'spec_helper.rb'
require './awesome_cache.rb'

describe AwesomeCache do
  before(:each) { cache.clear }
  let(:cache) { described_class.new(1, 1) }

  describe '#get' do
    context 'on first call' do
      it { expect(cache.get('a')).to eq nil }
    end

    context 'after set value' do
      before { cache.set('a', 'b') }
      it { expect(cache.get('a')).to eq 'b' }
    end

    context 'after set and clear' do
      before do
        cache.set('a', 'b')
        cache.clear
      end
      it { expect(cache.get('a')).to eq nil }
    end
  end

  describe '#set' do
    before do
      cache.set('a', 'b') # Записалось в memory store
      cache.set('b', 'c') # Записалось в memory store, 'a' => 'b' перенеслось в file_store
      cache.set('c', 'd') # Записалось в memory store, 'b' => 'c' перенеслось в file_store, 'a' => 'b' удалилось
    end

    it { expect(cache.get('b')).to eq 'c' }
    it { expect(cache.get('c')).to eq 'd' }
    it { expect(cache.get('a')).to eq nil }
  end

  describe '#clear' do
    it 'call clear on all stores' do
      expect_any_instance_of(Stores::MemoryStore).to receive(:clear)
      expect_any_instance_of(Stores::FileStore).to receive(:clear)
      cache.clear
    end
  end
end
