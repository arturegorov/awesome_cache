require 'spec_helper.rb'
require './stores/file_store.rb'

describe Stores::FileStore do
  let(:file_store) { described_class.new(1) }
  before(:each) { file_store.clear }

  describe '#cleanup' do
    before { file_store.set('a', 'b') }
    it { expect{ file_store.cleanup }.to change{ file_store.full? }.to false }
    it { expect(file_store.cleanup).to eq Hash('a' => 'b') }
  end

  describe '#full?' do
    context 'when less then max_size' do
      it { expect(file_store).to_not be_full }
    end

    context 'when eq max_size' do
      before { file_store.set('a', 'b') }
      it { expect(file_store).to be_full }
    end
  end

  describe '#get' do
    context 'when key not set yet' do
      it { expect(file_store.get('a')).to eq nil }
    end

    context 'when key already set' do
      before { file_store.set('a', 'b') }
      it { expect(file_store.get('a')).to eq 'b' }
    end
  end

  describe '#set' do
    it { expect { file_store.set('a', 'b') }.to change { file_store.send(:hash_from_file) }.to({'a' => 'b'}) }
  end

  describe '#clear' do
    before { file_store.set('a', 'b') }
    it { expect { file_store.clear }.to change { file_store.send(:hash_from_file).size }.to(0) }
  end
end
