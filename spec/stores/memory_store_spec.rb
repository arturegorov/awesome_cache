require 'spec_helper.rb'
require './stores/memory_store.rb'

describe Stores::MemoryStore do
  let(:memory_store) { described_class.new(1) }
  before(:each) { memory_store.clear }

  describe '#cleanup' do
    before { memory_store.set('a', 'b') }
    it { expect{ memory_store.cleanup }.to change{ memory_store.full? }.to false }
    it { expect(memory_store.cleanup).to eq Hash('a' => 'b') }
  end

  describe '#full?' do
    context 'when less then max_size' do
      it { expect(memory_store).to_not be_full }
    end

    context 'when eq max_size' do
      before { memory_store.set('a', 'b') }
      it { expect(memory_store).to be_full }
    end
  end

  describe '#get' do
    context 'when key not set yet' do
      it { expect(memory_store.get('a')).to eq nil }
    end

    context 'when key already set' do
      before { memory_store.set('a', 'b') }
      it { expect(memory_store.get('a')).to eq 'b' }
    end
  end

  describe '#set' do
    it { expect { memory_store.set('a', 'b') }.to change { memory_store.send(:store) }.to({'a' => 'b'}) }
  end

  describe '#clear' do
    before { memory_store.set('a', 'b') }
    it { expect { memory_store.clear }.to change { memory_store.send(:store).size }.to(0) }
  end
end
