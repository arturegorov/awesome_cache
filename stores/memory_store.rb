module Stores
  class MemoryStore < BaseStore
    def clear
      @store = {}
    end

    def cleanup
      last_pair = Array[store.to_a.last].to_h
      @store.delete(last_pair.keys.first)
      last_pair
    end

    def get(key)
      store[key]
    end

    def set(key, value)
      super
      store[key] = value
    end

    def full?
      store.size >= max_size
    end

    private

    def store
      @store ||= {}
    end
  end
end
