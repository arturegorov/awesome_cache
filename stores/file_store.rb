module Stores
  class FileStore < BaseStore
    FILE_PATH = 'tmp/file_store.txt'.freeze

    def clear
      File.open(FILE_PATH, 'w') {}
    end

    def cleanup
      new_hash = hash_from_file
      last_pair = Array[new_hash.to_a.last].to_h
      new_hash.delete(last_pair.keys.first)
      update_file(new_hash)
      last_pair
    end

    def get(key)
      hash_from_file[key.to_s]
    end

    def set(key, value)
      super
      new_hash = hash_from_file
      new_hash[key.to_s] = value
      update_file(new_hash)
    end

    def full?
      hash_from_file.size >= max_size
    end

    private

    def hash_from_file
      Hash[*File.read(FILE_PATH).split(/[, \n]+/)]
    end

    def update_file(new_hash)
      File.open(FILE_PATH, 'w') do |f|
        new_hash.each { |k, v| f << "#{k}, #{v}\n" }
      end
    end
  end
end
