module Stores
  class CacheFull < StandardError; end
  class BaseStore
    attr_reader :max_size

    def initialize(max_size)
      @max_size = max_size
    end

    def clear
      raise NotImplementedError
    end

    def cleanup
      raise NotImplementedError
    end

    def get(_key)
      raise NotImplementedError
    end

    def set(_key, _value)
      raise CacheFull.new('is full') if full?
    end

    def full?
      raise NotImplementedError
    end
  end
end
